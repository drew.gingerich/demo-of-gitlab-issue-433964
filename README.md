This repository is a demo for [GitLab.com issue 433964: Global variable doesn't expand in job variable of same name ](https://gitlab.com/gitlab-org/gitlab/-/issues/433964).
